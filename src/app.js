function sortTable(table, sortIndex) {
    let rows = [...table.querySelectorAll("tr")]

    let headRow = rows[0]
    let thead = document.createElement("thead")
    thead.append(headRow)
    rows = rows.splice(1)


    // Copy institution names into empty spots
    let currentName = ""
    rows.forEach(row => {
        if (row.children[0].innerHTML == "") {
            row.children[0].innerHTML = currentName
        } else {
            currentName = row.children[0].innerHTML
        }
    })

    let removed = [];
    if (window.location.pathname.includes("term")) {
        // Find non uniform rows
        rows = rows.filter((row) => {
            if (row.querySelectorAll(".special-line").length > 0 || row.children.length <= sortIndex || row.children[sortIndex].innerText == "") {
                removed.push(row)
                return false;
            }
            return true;
        })
    }

    rows = rows.sort((a, b) => {
        let pattern = /\d+\.\d+/;
        let number1 = pattern.exec(a.children[sortIndex].innerText);
        let number2 = pattern.exec(b.children[sortIndex].innerText);
        if (number1 == null || number2 == null) return 0;
        return Number.parseFloat(number1) - Number.parseFloat(number2)
    })
    rows = rows.reverse()
    table.innerHTML = ""
    table.append(thead)
    rows.forEach((row) => {
        table.append(row)
    })
    removed.forEach((row) => {
        table.append(row)
    })
}

let currentlySelected = null;
let originalHTML = ""

let startingIndex = 4;
let maxIndex = 5;

if (window.location.pathname.includes("term")) {
    startingIndex = 5;
    maxIndex = 11;
}

// If we are sorting term deposits, attach click events to column headings
document.querySelectorAll("thead tr").forEach((tr) => {
    for (let i = startingIndex; i < maxIndex; i++) {
        tr.style = "cursor:pointer"
        tr.children[i].addEventListener("click", (e) => {
            if (currentlySelected != null) {
                currentlySelected.innerHTML = originalHTML
            }

            originalHTML = e.target.innerHTML
            e.target.innerText += "↓"
            currentlySelected = e.target
            let table = e.target.parentElement.parentElement.parentElement
            sortTable(table, i)
        })
    }
})

document.querySelectorAll("h2").forEach(h2 => {
    let strong = document.createElement("strong");
    strong.textContent = "Note: To use the Rate Ranker extension just click on the column you want to sort by."
    strong.style.color = "red";
    h2.parentNode.insertBefore(strong, h2.nextSibling);
})